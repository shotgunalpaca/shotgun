﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
/// <summary>
/// Loads into the main menu after loading animation is finished
/// </summary>
public class AlpacaGames : MonoBehaviour {
    [SerializeField]
    float timeToNextScene;
	float timer;
    [SerializeField]
    string nextSceneName;
	// Use this for initialization
	void Start () {
	    if(nextSceneName == "")
        {
            Debug.LogError("Input a scene name into Alpaca Games script");
        }
	}


	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
		if(timer > timeToNextScene){
            SceneManager.LoadScene(nextSceneName);
		}
	
	}
}
