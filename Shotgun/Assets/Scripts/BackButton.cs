﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class BackButton : MonoBehaviour {
    [SerializeField]
    string backSceneName;
    void Start()
    {
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            Back();
        }
    }
    public void Back()
    {
        if (backSceneName == "")
        {
            Application.Quit();
        }
        else
        {
            SceneManager.LoadScene(backSceneName);
        }        
    }
}
