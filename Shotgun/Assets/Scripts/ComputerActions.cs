﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class ComputerActions : MonoBehaviour {
    int compBullets = 0;
    int blockCounter = 0;
    [SerializeField]
    int maxBlocksInRow = 5;
    public readonly int RELOAD = 0;
    public readonly int SHOOT = 1;
    public readonly int BLOCK = 2;
    public readonly int TOTAL = 3;
    public bool isHardMode;
    [SerializeField]
    List<int> p1c1r;
    [SerializeField]
    List<int> p1c1s;
    [SerializeField]
    List<int> p1c1b;
    [SerializeField]
    List<int> p1c0r;
    [SerializeField]
    List<int> p1c0s;
    [SerializeField]
    List<int> p1c0b;
    [SerializeField]
    List<int> p0c1s;
    [SerializeField]
    List<int> p0c1b;
    [SerializeField]
    Text bulletsText;
    int prevPlayerAction;

    bool defCanKillPlayer = false;
    // Use this for initialization
    void Start () {
        LoadPlayerPrefs();
        compBullets = 0;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public int GetCompAction(int playerAction, int playerBullets, int playerBlockCounter)
    {
        int compAction = Analyze(playerAction, playerBullets, playerBlockCounter);
        prevPlayerAction = playerAction;
        if (compAction == RELOAD)
        {
            compBullets++;
            blockCounter = 0;
        }
        else if (compAction == SHOOT)
        {
            compBullets--;
            blockCounter = 0;
        }
        else if (compAction == BLOCK)
        {
            blockCounter++;
        }
        bulletsText.text = "Bullets: " + compBullets;
        return compAction;
    }

    void LoadPlayerPrefs()
    {
        p1c1r = new List<int>();
        p1c1s = new List<int>();
        p1c1b = new List<int>();
        p1c0r = new List<int>();
        p1c0s = new List<int>();
        p1c0b = new List<int>();
        p0c1s = new List<int>();
        p0c1b = new List<int>();
        //p1c1
        p1c1r.Add(PlayerPrefs.GetInt("p1c1r0"));
        p1c1r.Add(PlayerPrefs.GetInt("p1c1r1"));
        p1c1r.Add(PlayerPrefs.GetInt("p1c1r2"));

        p1c1s.Add(PlayerPrefs.GetInt("p1c1s0"));
        p1c1s.Add(PlayerPrefs.GetInt("p1c1s1"));
        p1c1s.Add(PlayerPrefs.GetInt("p1c1s2"));

        p1c1b.Add(PlayerPrefs.GetInt("p1c1b0"));
        p1c1b.Add(PlayerPrefs.GetInt("p1c1b1"));
        p1c1b.Add(PlayerPrefs.GetInt("p1c1b2"));
        
        //p1c0
        p1c0r.Add(PlayerPrefs.GetInt("p1c0r0"));
        p1c0r.Add(PlayerPrefs.GetInt("p1c0r1"));
        p1c0r.Add(PlayerPrefs.GetInt("p1c0r2"));

        p1c0s.Add(PlayerPrefs.GetInt("p1c0s0"));
        p1c0s.Add(PlayerPrefs.GetInt("p1c0s1"));
        p1c0s.Add(PlayerPrefs.GetInt("p1c0s2"));

        p1c0b.Add(PlayerPrefs.GetInt("p1c0b0"));
        p1c0b.Add(PlayerPrefs.GetInt("p1c0b1"));
        p1c0b.Add(PlayerPrefs.GetInt("p1c0b2"));

        //p0c1
        p0c1s.Add(PlayerPrefs.GetInt("p0c1s0"));
        p0c1s.Add(PlayerPrefs.GetInt("p0c1s1"));
        p0c1s.Add(PlayerPrefs.GetInt("p0c1s2"));

        p0c1b.Add(PlayerPrefs.GetInt("p0c1b0"));
        p0c1b.Add(PlayerPrefs.GetInt("p0c1b1"));
        p0c1b.Add(PlayerPrefs.GetInt("p0c1b2"));
    }

    public void SavePlayerPrefs()
    {
        //p1c1
        PlayerPrefs.SetInt("p1c1r0", p1c1r[RELOAD]);
        PlayerPrefs.SetInt("p1c1r1", p1c1r[SHOOT]);
        PlayerPrefs.SetInt("p1c1r2", p1c1r[BLOCK]);

        PlayerPrefs.SetInt("p1c1s0", p1c1s[RELOAD]);
        PlayerPrefs.SetInt("p1c1s1", p1c1s[SHOOT]);
        PlayerPrefs.SetInt("p1c1s2", p1c1s[BLOCK]);

        PlayerPrefs.SetInt("p1c1b0", p1c1b[RELOAD]);
        PlayerPrefs.SetInt("p1c1b1", p1c1b[SHOOT]);
        PlayerPrefs.SetInt("p1c1b2", p1c1b[BLOCK]);

        //p1c0
        PlayerPrefs.SetInt("p1c0r0", p1c0r[RELOAD]);
        PlayerPrefs.SetInt("p1c0r1", p1c0r[SHOOT]);
        PlayerPrefs.SetInt("p1c0r2", p1c0r[BLOCK]);

        PlayerPrefs.SetInt("p1c0s0", p1c0s[RELOAD]);
        PlayerPrefs.SetInt("p1c0s1", p1c0s[SHOOT]);
        PlayerPrefs.SetInt("p1c0s2", p1c0s[BLOCK]);

        PlayerPrefs.SetInt("p1c0b0", p1c0b[RELOAD]);
        PlayerPrefs.SetInt("p1c0b1", p1c0b[SHOOT]);
        PlayerPrefs.SetInt("p1c0b2", p1c0b[BLOCK]);

        //p0c1
        PlayerPrefs.SetInt("p0c1s0", p0c1s[RELOAD]);
        PlayerPrefs.SetInt("p0c1s1", p0c1s[SHOOT]);
        PlayerPrefs.SetInt("p0c1s2", p0c1s[BLOCK]);

        PlayerPrefs.SetInt("p0c1b0", p0c1b[RELOAD]);
        PlayerPrefs.SetInt("p0c1b1", p0c1b[SHOOT]);
        PlayerPrefs.SetInt("p0c1b2", p0c1b[BLOCK]);
        PlayerPrefs.Save();
    }

    public void ResetPlayerPrefs()
    {
        //p1c1
        PlayerPrefs.SetInt("p1c1r0", 0);
        PlayerPrefs.SetInt("p1c1r1", 0);
        PlayerPrefs.SetInt("p1c1r2", 0);

        PlayerPrefs.SetInt("p1c1s0", 0);
        PlayerPrefs.SetInt("p1c1s1", 0);
        PlayerPrefs.SetInt("p1c1s2", 0);

        PlayerPrefs.SetInt("p1c1b0", 0);
        PlayerPrefs.SetInt("p1c1b1", 0);
        PlayerPrefs.SetInt("p1c1b2", 0);

        //p1c0
        PlayerPrefs.SetInt("p1c0r0", 0);
        PlayerPrefs.SetInt("p1c0r1", 0);
        PlayerPrefs.SetInt("p1c0r2", 0);

        PlayerPrefs.SetInt("p1c0s0", 0);
        PlayerPrefs.SetInt("p1c0s1", 0);
        PlayerPrefs.SetInt("p1c0s2", 0);

        PlayerPrefs.SetInt("p1c0b0", 0);
        PlayerPrefs.SetInt("p1c0b1", 0);
        PlayerPrefs.SetInt("p1c0b2", 0);

        //p0c1
        PlayerPrefs.SetInt("p0c1s0", 0);
        PlayerPrefs.SetInt("p0c1s1", 0);
        PlayerPrefs.SetInt("p0c1s2", 0);

        PlayerPrefs.SetInt("p0c1b0", 0);
        PlayerPrefs.SetInt("p0c1b1", 0);
        PlayerPrefs.SetInt("p0c1b2", 0);

        PlayerPrefs.SetInt("TotalPlayerWin", 0);
        PlayerPrefs.SetInt("MediumPlayerWin", 0);
        PlayerPrefs.SetInt("HardPlayerWin", 0);
        PlayerPrefs.SetInt("TotalComputerWin", 0);
        PlayerPrefs.SetInt("MediumComputerWin", 0);
        PlayerPrefs.SetInt("HardComputerWin", 0);
        PlayerPrefs.Save();
        LoadPlayerPrefs();
    }

    //calculate probability of player's next move
    //TODO: Change probabilities when it guesses correctly or incorrectly
    //Idea: Increase by 1 when correct and Decrease by 1 when incorrect.
    int Calculate(List<int> RSB, int playerBullets, int playerBlockCounter)
    {
        
        int total = RSB[RELOAD] + RSB[SHOOT] + RSB[BLOCK];
        //Debug.Log("Total: " + total);
       // Debug.Log("Rand: " + rand);
        if (total <= 0)
        {
            Debug.Log("Can't Predict");
            if (playerBullets > 0)
            {
                if (compBullets > 0)
                {
                    return Random.Range(0, 3);
                }
                else
                {
                    //50% reload or 50% block
                    int reloadOrBlock = Random.Range(0, 2);
                    //change shoot to block
                    if(reloadOrBlock == 1)
                    {
                        reloadOrBlock = 2;
                    }
                    return reloadOrBlock;
                }
            }
            //don't block if player has no bullets
            else
            {
                return Random.Range(0, 2);
            }
        }
        
        float reload = RSB[RELOAD] / (float)total;
        float shoot = RSB[SHOOT] / (float)total;
        float block = RSB[BLOCK] / (float)total;
        //Debug.Log("Reload: " + reload);
        //Debug.Log("Shoot: " + (reload+ shoot));
        //Debug.Log("Block: " + (shoot+block+reload));

        //Predict player's next move
        float rand;
        bool playerCantBlock = false;
        if(playerBlockCounter >= maxBlocksInRow)
        {
            playerCantBlock = true;
            Debug.Log("Player Can't Block");
        }
        //if player has no bullets, then ensure we don't block
        //so we know that the player can't shoot
        //and if we predict the player blocking, we should reload       
        if (playerBullets <= 0)
        {
            rand = Random.Range(0.0f, reload + block);
            //Predict Player reloads
            if (rand < reload || playerCantBlock)
            {
                Debug.Log("Predicted Reload");
                if (compBullets > 0)
                {
                    return SHOOT;
                }
                else
                {
                    return RELOAD;
                }
            }
            //Predict Player blocks
            else
            {
                Debug.Log("Predicted Block");
                return RELOAD;
            }
        }
        //if player can't block. only predict player can reload or shoot
        else if (playerCantBlock)
        {
            rand = Random.Range(0.0f, reload + shoot);
            //Predict Player reloads
            if (rand < reload)
            {
                Debug.Log("Predicted Reload");
                if (compBullets > 0)
                {
                    return SHOOT;
                }
                else
                {
                    return RELOAD;
                }
            }
            //Predict Player shoots
            else
            {
                Debug.Log("Predicted Shoot");
                if (blockCounter < maxBlocksInRow)
                {
                    return BLOCK;
                }
                else if (compBullets > 0)
                {
                    return SHOOT;
                }
                else
                {
                    return RELOAD;
                }
            }
        }
        else
        {
            rand = Random.Range(0.0f, 1.0f);
            //Testing out computer more likely to shoot when it has more bullets by increasing reload percantage, so it becomes more threatening if it has more bullets
            //Testing out player is more likely to shoot when the player has more bullets by increasing shoot percentage
            float compMoreLikelyToShoot = 0;
            float playerMoreLikelyToShoot = 0;
            if (isHardMode)
            {
                compMoreLikelyToShoot = .01f * compBullets * compBullets;
                playerMoreLikelyToShoot = .005f * playerBullets * playerBullets;
            }
           
            //Predict Player reloads
            if (rand < reload + compMoreLikelyToShoot)
            {
                Debug.Log("Predicted Reload");
                if (compBullets > 0)
                {
                    return SHOOT;
                }
                else
                {
                    return RELOAD;
                }
            }
            //Predict Player shoots
            else if (rand < reload + shoot + compMoreLikelyToShoot + playerMoreLikelyToShoot)
            {
                Debug.Log("Predicted Shoot");
                if (blockCounter < maxBlocksInRow)
                {
                    return BLOCK;
                }
                else if (compBullets > 0)
                {
                    return SHOOT;
                }
                else
                {
                    return RELOAD;
                }
            }
            //Predict Player blocks
            else
            {
                Debug.Log("Predicted Block");
                //first check if player has bullets
                if (playerBullets <= 0)
                {
                    return RELOAD;
                }
                //Old: 15% chance to reload. 85% chance to block
                //New: reload based on how percentage player has blocked
                if (isHardMode)
                {
                    float predictBlock = block * .3f + blockCounter * .1f;
                    //Debug.Log("blocK: " + predictBlock);
                    float rand2 = Random.Range(0.0f, 1.0f);
                    //Debug.Log("Rand2: " + rand2);
                    if (rand2 < predictBlock)
                    {
                        return RELOAD;
                    }
                    else
                    {
                        return BLOCK;
                    }
                }
                //not hard mode, just reload. pure prediction. no weights
                return RELOAD;

            }
        }                
    }

    //Predict player's actoins based on previous moves
    int Analyze(int playerAction, int playerBullets, int playerBlockCounter)
    {
       
        int compAction = 0;
        //Special case 1. no bullets on both sides
        if(playerBullets == 0 && compBullets == 0)
        {
         //   Debug.Log("Scenario 0 0");
            Debug.Log("Predicted Reload");
            return RELOAD;
        }
        
        if (playerBullets >= 1 && compBullets >= 1)
        {
           // Debug.Log("Scenario 1 1");
            //player and computer has at least 1 bullet after reload
            if (prevPlayerAction == RELOAD)
            {
                
                compAction = Calculate(p1c1r, playerBullets, playerBlockCounter);
                p1c1r[playerAction]++;
            }
            //player and computer has at least 1 bullet after shoot
            else if (prevPlayerAction == SHOOT)
            {
                compAction = Calculate(p1c1s, playerBullets, playerBlockCounter);
                p1c1s[playerAction]++;
            }
            //player and computer has at least 1 bullet after block
            else if (prevPlayerAction == BLOCK)
            {
                compAction = Calculate(p1c1b, playerBullets, playerBlockCounter);
                p1c1b[playerAction]++;
            }
            return compAction;
        }



        //player has 1 bullet and computer has 0 after reload
        //player has 1 bullet and computer has 0 after shoot
        //player has 1 bullet and computer has 0 after block
        if (playerBullets >= 1 && compBullets == 0)
        {
          //  Debug.Log("Scenario 1 0");
            //player and computer has at least 1 bullet after reload
            if (prevPlayerAction == RELOAD)
            {
                compAction = Calculate(p1c0r, playerBullets, playerBlockCounter);
                p1c0r[playerAction]++;
            }
            //player and computer has at least 1 bullet after shoot
            else if (prevPlayerAction == SHOOT)
            {
                compAction = Calculate(p1c0s,playerBullets, playerBlockCounter);
                p1c0s[playerAction]++;
            }
            //player and computer has at least 1 bullet after block
            else if (prevPlayerAction == BLOCK)
            {
                compAction = Calculate(p1c0b, playerBullets, playerBlockCounter);
                p1c0b[playerAction]++;
            }
            return compAction;
        }

        //player has 0 bullets and computer has 1 after block
        //player has 0 bullets and computer has 1 after shoot
        if (playerBullets == 0 && compBullets >= 1)
        {
            //Special Case 2. Computer has enough bullets to kill the player for sure
            //Computer has bullets > player can block
            if(compBullets > maxBlocksInRow - blockCounter)
            {
                defCanKillPlayer = true;
                Debug.Log("Can definitely kill player");
            }
            //  Debug.Log("Scenario 0 1");
            if (prevPlayerAction == SHOOT)
            {
                compAction = Calculate(p0c1s, playerBullets, playerBlockCounter);
                p0c1s[playerAction]++;
            }
            //player and computer has at least 1 bullet after block
            else if (prevPlayerAction == BLOCK)
            {
                compAction = Calculate(p0c1b, playerBullets, playerBlockCounter);
                p0c1b[playerAction]++;
            }
            if (defCanKillPlayer)
            {
                return SHOOT;
            }
            return compAction;
        }

        Debug.Log("Skipped all my if statements");
        return RELOAD;
    }
    public void SetDefCanKillPlayer(bool val)
    {
        defCanKillPlayer = val;
    }
}
