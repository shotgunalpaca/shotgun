﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GameModeButtons : MonoBehaviour {
    public void Easy()
    {
        SceneManager.LoadScene("Hard Mode");
    }

    public void Medium()
    {
        SceneManager.LoadScene("Medium Mode");
    }

    public void Hard()
    {
        SceneManager.LoadScene("Hard Mode");
    }


}
