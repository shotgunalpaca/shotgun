﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuButtons : MonoBehaviour {
    public void Play()
    {
        SceneManager.LoadScene("Game Mode");
    }

    public void ScoreScreen()
    {
        SceneManager.LoadScene("Score");
    }

    public void InstructionsScreen()
    {
        SceneManager.LoadScene("Instructions");
    }

    public void CreditsScreen()
    {
        SceneManager.LoadScene("Credits");
    }

    public void ExitGame()
    {
        Application.Quit();
    }

}
