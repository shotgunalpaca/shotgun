﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class PlayerActions : MonoBehaviour {
    [SerializeField]
    Image playerImage;
    [SerializeField]
    Image compImage;

    [SerializeField]
    Text bulletsText;
    [SerializeField]
    Text gameOverText;

    [SerializeField]
    Sprite reloadSprite;
    [SerializeField]
    Sprite shootSprite;
    [SerializeField]
    Sprite blockSprite;

    int numBullets = 0;
    int blockCounter = 0;
    [SerializeField]
    int maxBlocksInRow = 5;

    [SerializeField]
    GameObject reloadButton;
    [SerializeField]
    GameObject shootButton;
    [SerializeField]
    GameObject blockButton;


    public readonly int RELOAD = 0;
    public readonly int SHOOT = 1;
    public readonly int BLOCK = 2;

    [SerializeField]
    ComputerActions compActionScript;
    bool gameOver = false;

    //public int playerWinCount;
    //public int compWinCount;
    // Use this for initialization
    void Start () {
        shootButton.SetActive(false);
        //playerWinCount = PlayerPrefs.GetInt("PlayerWin");
        //compWinCount = PlayerPrefs.GetInt("ComputerWin");
    }
	
	// Update is called once per frame
	void Update () {
	}
    public void MainMenu()
    {
        compActionScript.SavePlayerPrefs();
        SceneManager.LoadScene("Main Menu");
    }
    public void Restart()
    {
        compActionScript.SavePlayerPrefs();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void Reload()
    {
        ComputerAction(RELOAD);

        playerImage.sprite = reloadSprite;
        numBullets++;
        bulletsText.text = "Bullets: " + numBullets;

        if (!gameOver)
        {
            if (!shootButton.activeSelf)
                shootButton.SetActive(true);
            blockCounter = 0;
            if (!blockButton.activeSelf)
                blockButton.SetActive(true);
        }

        
    }
    public void Shoot()
    {
        ComputerAction(SHOOT);

        playerImage.sprite = shootSprite;
        numBullets--;
        bulletsText.text = "Bullets: " + numBullets;

        if (!gameOver)
        {
            if (numBullets <= 0)
            {
                shootButton.SetActive(false);
            }

            blockCounter = 0;
            if (!blockButton.activeSelf)
                blockButton.SetActive(true);
        }

        
    }
    public void Block()
    {
        ComputerAction(BLOCK);
        playerImage.sprite = blockSprite;
        if (!gameOver)
        {
            blockCounter++;
            if (blockCounter >= maxBlocksInRow)
            {
                blockButton.SetActive(false);
            }
        }
        
    }
    void ComputerAction(int playerAction)
    {
        int compAction = compActionScript.GetCompAction(playerAction, numBullets, blockCounter);
        if(compAction == RELOAD)
        {
            compImage.sprite = reloadSprite;
            if(playerAction == SHOOT)
            {
                GameOver(true);
            }
        } else if(compAction == SHOOT)
        {
            compImage.sprite = shootSprite;
            if(playerAction == RELOAD)
            {
                GameOver(false);
            }
        }
        else if(compAction == BLOCK)
        {
            compImage.sprite = blockSprite;
        }
    }

    void GameOver(bool playerWin)
    {
        gameOver = true;
        reloadButton.SetActive(false);
        shootButton.SetActive(false);
        blockButton.SetActive(false);
        if (playerWin)
        {
            gameOverText.text = "You Win";
            PlayerPrefs.SetInt("TotalPlayerWin", PlayerPrefs.GetInt("TotalPlayerWin")+1);
            
            if (compActionScript.isHardMode)
            {
                PlayerPrefs.SetInt("HardPlayerWin", PlayerPrefs.GetInt("HardPlayerWin") + 1);
            }
            else
            {
                PlayerPrefs.SetInt("MediumPlayerWin", PlayerPrefs.GetInt("MediumPlayerWin") + 1);
            }
            
            //Debug.Log("WIN " + PlayerPrefs.GetInt("TotalPlayerWin") + " " + PlayerPrefs.GetInt("MediumPlayerWin") + " " + PlayerPrefs.GetInt("HardPlayerWin"));
        }
        else
        {
            gameOverText.text = "Game Over";
            PlayerPrefs.SetInt("TotalComputerWin", PlayerPrefs.GetInt("TotalComputerWin") + 1);
            if (compActionScript.isHardMode)
            {
                PlayerPrefs.SetInt("HardComputerWin", PlayerPrefs.GetInt("HardComputerWin") + 1);
            }
            else
            {
                PlayerPrefs.SetInt("MediumComputerWin", PlayerPrefs.GetInt("MediumComputerWin") + 1);
            }
            
            
        }
        
        compActionScript.SavePlayerPrefs();
        compActionScript.SetDefCanKillPlayer(false);
        //playerWinCount = PlayerPrefs.GetInt("PlayerWin");
        //compWinCount = PlayerPrefs.GetInt("ComputerWin");
    }


}
