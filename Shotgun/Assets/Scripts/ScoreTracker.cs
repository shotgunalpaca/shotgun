﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ScoreTracker : MonoBehaviour {
    [SerializeField]
    Text totalGamesWonText;
    [SerializeField]
    Text totalGamesLostText;
    [SerializeField]
    Text totalGamePercentText;

    [SerializeField]
    Text mediumGamesWonText;
    [SerializeField]
    Text mediumGamesLostText;
    [SerializeField]
    Text mediumGamePercentText;

    [SerializeField]
    Text hardGamesWonText;
    [SerializeField]
    Text hardGamesLostText;
    [SerializeField]
    Text hardGamePercentText;
    // Use this for initialization
    void Start () {
        int totalGamesWon = PlayerPrefs.GetInt("TotalPlayerWin");
        int totalGamesLost = PlayerPrefs.GetInt("TotalComputerWin");
        CalculateGameWinLoss(totalGamesWonText, totalGamesWon,totalGamesLostText, totalGamesLost, totalGamePercentText);

        int mediumGamesWon = PlayerPrefs.GetInt("MediumPlayerWin");
        int mediumGamesLost = PlayerPrefs.GetInt("MediumComputerWin");
        CalculateGameWinLoss(mediumGamesWonText, mediumGamesWon, mediumGamesLostText, mediumGamesLost, mediumGamePercentText);

        int hardGamesWon = PlayerPrefs.GetInt("HardPlayerWin");
        int hardGamesLost = PlayerPrefs.GetInt("HardComputerWin");
        CalculateGameWinLoss(hardGamesWonText, hardGamesWon, hardGamesLostText, hardGamesLost, hardGamePercentText);

        //Debug.Log(totalGamesWon + " " + mediumGamesWon + " " + hardGamesWon);

    }
	
    void CalculateGameWinLoss(Text gamesWonText, int gamesWon, Text gamesLostText, int gamesLost, Text gamePercentText)
    {
        
        float gamesWonPercent = (float)gamesWon / (float)(gamesWon + gamesLost) * 100;
        gamesWonText.text = "Games Won: " + gamesWon;
        gamesLostText.text = "Games Lost: " + gamesLost;
        if ((gamesWon + gamesLost) == 0)
        {
            gamesWonPercent = 0;
        }
        gamePercentText.text = "% Games Won: " + gamesWonPercent.ToString("0.00") + "%";
    }
	// Update is called once per frame
	void Update () {
		
	}
}
