﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class StartScreenButtons : MonoBehaviour {

	public void Play(){
		SceneManager.LoadScene ("Level 1");
	}

	public void ReturntoStartScreen(){
        SceneManager.LoadScene("Start Screen");
	}

	public void InstructionsScreen(){
        SceneManager.LoadScene("Instructions Page");
	}

	public void CreditsScreen(){
        SceneManager.LoadScene("Credits Page");
	}

	public void ExitGame(){
		Application.Quit();
	}


}
